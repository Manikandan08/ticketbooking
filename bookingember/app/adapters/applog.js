import RESTAdapter from '@ember-data/adapter/rest';

export default class ApplogAdapter extends RESTAdapter {
  namespace = 'thetareticketbook';
}
