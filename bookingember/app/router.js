import EmberRouter from '@ember/routing/router';
import config from 'moviebooking/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('mainpage');
  this.route('adam');
  this.route('avatar');
  this.route('sky');
  this.route('wonder');
  this.route('bookticket');
  this.route('adampage');
  this.route('avathar');
  this.route('payamount');
  this.route('avatarbook');
  this.route('successful');
});
