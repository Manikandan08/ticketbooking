import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
export default class AdamComponent extends Component {
  @service router;
  @action adam(page) {
    this.router.transitionTo(page);
  }
}
