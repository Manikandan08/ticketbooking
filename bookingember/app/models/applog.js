import Model, { attr } from '@ember-data/model';

export default class ApplogModel extends Model {
  @attr('string') number;
  @attr('string') email;
  @attr ('string') count;
  @attr ('string') total; 
   // @attr ('string') userid;
}
