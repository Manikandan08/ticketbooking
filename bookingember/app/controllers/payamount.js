import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
export default class PayamountController extends Controller {
  @tracked email = '';
  @tracked number = '';
  @tracked regexnum = false;
  @tracked regexemail = false;
  @tracked num = '';
  @tracked mailstr = '';
  @service store;
  @action add() {
    this.regexnum = /^[6-9]\d{9}$/.test(this.number);
    this.regexemail =
      /^[a-z0-9-]+(\.[_a-z0-9]+)*@[a-z0-9]+(\.[a-z0-9]+)*(\.[a-z]{2,4})$/.test(
        this.email
      );
    this.validfnum();
    this.validemail();
    if (this.num == 'valid' && this.mailstr == 'valid') {
      console.log(this.number + this.email);
      const userlist = this.store.createRecord('applog', {
        number: this.number,
        email: this.email,
        // user:this.userid,
      });
      userlist.save();
      this.transitionToRoute('successful');
    }
  }
  validfnum() {
    if (!this.number) {
      this.num = 'empty';
    } else if (this.regexnum) {
      this.num = 'valid';
    } else this.num = 'invalid';
  }
  validemail() {
    if (!this.email) {
      this.mailstr = 'empty';
    } else if (this.regexemail) {
      // this.transitionToRoute('formsuccess');
      this.mailstr = 'valid';
    } else this.mailstr = 'invalid';
  }
}
