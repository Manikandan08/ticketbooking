import Controller from '@ember/controller';
export default class MainpageController extends Controller {
  array = [
    { image: 'assets/movie1.jpeg', val: 'black Adam', page: 'adampage' },
    { image: 'assets/movie2.jpg', val: 'AVATAR', page: 'avathar' },
    { image: 'assets/movie3.jpg', val: 'SKY', page: 'sky' },
    { image: 'assets/movie4.jpeg', val: 'WONDER WOMEN', page: 'wonder' },
  ];
  topic = [{ image: 'assets/movie1.jpeg', value: 'blackadam' }];
}
