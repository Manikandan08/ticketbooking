import Controller from '@ember/controller';
import { action } from '@ember/object';
export default class AdamController extends Controller {
  @action book() {
    this.transitionToRoute('bookticket');
  }
}
