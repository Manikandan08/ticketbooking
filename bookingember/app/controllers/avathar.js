import Controller from '@ember/controller';
import { action } from '@ember/object';
export default class AvatharController extends Controller {
  @action avatar() {
    this.transitionToRoute('avatarbook');
  }
}
