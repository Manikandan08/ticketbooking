import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
export default class ApplicationController extends Controller {
  @service store;
  beforeModel() {
    return this.store
      .findAll('applog')
      .then(() => {
        this.transitionTo('successful');
      })
      .catch(() => {
        this.transitionTo('mainpage');
      });
  }
}
