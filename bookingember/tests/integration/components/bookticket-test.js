import { module, test } from 'qunit';
import { setupRenderingTest } from 'moviebooking/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | bookticket', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Bookticket />`);

    assert.dom(this.element).hasText('');

    // Template block usage:
    await render(hbs`
      <Bookticket>
        template block text
      </Bookticket>
    `);

    assert.dom(this.element).hasText('template block text');
  });
});
