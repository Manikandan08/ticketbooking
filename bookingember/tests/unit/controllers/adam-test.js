import { module, test } from 'qunit';
import { setupTest } from 'moviebooking/tests/helpers';

module('Unit | Controller | adam', function (hooks) {
  setupTest(hooks);

  // TODO: Replace this with your real tests.
  test('it exists', function (assert) {
    let controller = this.owner.lookup('controller:adam');
    assert.ok(controller);
  });
});
