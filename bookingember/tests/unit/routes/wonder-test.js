import { module, test } from 'qunit';
import { setupTest } from 'moviebooking/tests/helpers';

module('Unit | Route | wonder', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:wonder');
    assert.ok(route);
  });
});
