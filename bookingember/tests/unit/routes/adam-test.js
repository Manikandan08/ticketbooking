import { module, test } from 'qunit';
import { setupTest } from 'moviebooking/tests/helpers';

module('Unit | Route | adam', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:adam');
    assert.ok(route);
  });
});
