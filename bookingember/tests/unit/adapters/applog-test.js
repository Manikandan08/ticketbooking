import { module, test } from 'qunit';
import { setupTest } from 'moviebooking/tests/helpers';

module('Unit | Adapter | applog', function (hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function (assert) {
    let adapter = this.owner.lookup('adapter:applog');
    assert.ok(adapter);
  });
});
