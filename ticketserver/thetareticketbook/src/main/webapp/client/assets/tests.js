'use strict';

define("moviebooking/tests/helpers/index", ["exports", "ember-qunit"], function (_exports, _emberQunit) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.setupApplicationTest = setupApplicationTest;
  _exports.setupRenderingTest = setupRenderingTest;
  _exports.setupTest = setupTest;
  0; //eaimeta@70e063a35619d71f0,"ember-qunit"eaimeta@70e063a35619d71f
  // This file exists to provide wrappers around ember-qunit's / ember-mocha's
  // test setup functions. This way, you can easily extend the setup that is
  // needed per test type.

  function setupApplicationTest(hooks, options) {
    (0, _emberQunit.setupApplicationTest)(hooks, options);

    // Additional setup for application tests can be done here.
    //
    // For example, if you need an authenticated session for each
    // application test, you could do:
    //
    // hooks.beforeEach(async function () {
    //   await authenticateSession(); // ember-simple-auth
    // });
    //
    // This is also a good place to call test setup functions coming
    // from other addons:
    //
    // setupIntl(hooks); // ember-intl
    // setupMirage(hooks); // ember-cli-mirage
  }

  function setupRenderingTest(hooks, options) {
    (0, _emberQunit.setupRenderingTest)(hooks, options);

    // Additional setup for rendering tests can be done here.
  }

  function setupTest(hooks, options) {
    (0, _emberQunit.setupTest)(hooks, options);

    // Additional setup for unit tests can be done here.
  }
});
define("moviebooking/tests/integration/components/adam-test", ["@ember/template-factory", "qunit", "moviebooking/tests/helpers", "@ember/test-helpers"], function (_templateFactory, _qunit, _helpers, _testHelpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers",0,"@ember/test-helpers",0,"ember-cli-htmlbars"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Integration | Component | adam', function (hooks) {
    (0, _helpers.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)((0, _templateFactory.createTemplateFactory)(
      /*
        <Adam />
      */
      {
        "id": "e6dfaFQv",
        "block": "[[[8,[39,0],null,null,null]],[],false,[\"adam\"]]",
        "moduleName": "(unknown template module)",
        "isStrictMode": false
      }));
      assert.dom(this.element).hasText('');

      // Template block usage:
      await (0, _testHelpers.render)((0, _templateFactory.createTemplateFactory)(
      /*
        
            <Adam>
              template block text
            </Adam>
          
      */
      {
        "id": "m/7URFfw",
        "block": "[[[1,\"\\n      \"],[8,[39,0],null,null,[[\"default\"],[[[[1,\"\\n        template block text\\n      \"]],[]]]]],[1,\"\\n    \"]],[],false,[\"adam\"]]",
        "moduleName": "(unknown template module)",
        "isStrictMode": false
      }));
      assert.dom(this.element).hasText('template block text');
    });
  });
});
define("moviebooking/tests/integration/components/adampage-test", ["@ember/template-factory", "qunit", "moviebooking/tests/helpers", "@ember/test-helpers"], function (_templateFactory, _qunit, _helpers, _testHelpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers",0,"@ember/test-helpers",0,"ember-cli-htmlbars"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Integration | Component | adampage', function (hooks) {
    (0, _helpers.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)((0, _templateFactory.createTemplateFactory)(
      /*
        <Adampage />
      */
      {
        "id": "gOMhmEdi",
        "block": "[[[8,[39,0],null,null,null]],[],false,[\"adampage\"]]",
        "moduleName": "(unknown template module)",
        "isStrictMode": false
      }));
      assert.dom(this.element).hasText('');

      // Template block usage:
      await (0, _testHelpers.render)((0, _templateFactory.createTemplateFactory)(
      /*
        
            <Adampage>
              template block text
            </Adampage>
          
      */
      {
        "id": "vBc6JZS3",
        "block": "[[[1,\"\\n      \"],[8,[39,0],null,null,[[\"default\"],[[[[1,\"\\n        template block text\\n      \"]],[]]]]],[1,\"\\n    \"]],[],false,[\"adampage\"]]",
        "moduleName": "(unknown template module)",
        "isStrictMode": false
      }));
      assert.dom(this.element).hasText('template block text');
    });
  });
});
define("moviebooking/tests/integration/components/bookticket-test", ["@ember/template-factory", "qunit", "moviebooking/tests/helpers", "@ember/test-helpers"], function (_templateFactory, _qunit, _helpers, _testHelpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers",0,"@ember/test-helpers",0,"ember-cli-htmlbars"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Integration | Component | bookticket', function (hooks) {
    (0, _helpers.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)((0, _templateFactory.createTemplateFactory)(
      /*
        <Bookticket />
      */
      {
        "id": "MxjicJBv",
        "block": "[[[8,[39,0],null,null,null]],[],false,[\"bookticket\"]]",
        "moduleName": "(unknown template module)",
        "isStrictMode": false
      }));
      assert.dom(this.element).hasText('');

      // Template block usage:
      await (0, _testHelpers.render)((0, _templateFactory.createTemplateFactory)(
      /*
        
            <Bookticket>
              template block text
            </Bookticket>
          
      */
      {
        "id": "78cP2Cv3",
        "block": "[[[1,\"\\n      \"],[8,[39,0],null,null,[[\"default\"],[[[[1,\"\\n        template block text\\n      \"]],[]]]]],[1,\"\\n    \"]],[],false,[\"bookticket\"]]",
        "moduleName": "(unknown template module)",
        "isStrictMode": false
      }));
      assert.dom(this.element).hasText('template block text');
    });
  });
});
define("moviebooking/tests/integration/components/pagerender-test", ["@ember/template-factory", "qunit", "moviebooking/tests/helpers", "@ember/test-helpers"], function (_templateFactory, _qunit, _helpers, _testHelpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers",0,"@ember/test-helpers",0,"ember-cli-htmlbars"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Integration | Component | pagerender', function (hooks) {
    (0, _helpers.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)((0, _templateFactory.createTemplateFactory)(
      /*
        <Pagerender />
      */
      {
        "id": "rwDnSQi/",
        "block": "[[[8,[39,0],null,null,null]],[],false,[\"pagerender\"]]",
        "moduleName": "(unknown template module)",
        "isStrictMode": false
      }));
      assert.dom(this.element).hasText('');

      // Template block usage:
      await (0, _testHelpers.render)((0, _templateFactory.createTemplateFactory)(
      /*
        
            <Pagerender>
              template block text
            </Pagerender>
          
      */
      {
        "id": "YLWqsCkZ",
        "block": "[[[1,\"\\n      \"],[8,[39,0],null,null,[[\"default\"],[[[[1,\"\\n        template block text\\n      \"]],[]]]]],[1,\"\\n    \"]],[],false,[\"pagerender\"]]",
        "moduleName": "(unknown template module)",
        "isStrictMode": false
      }));
      assert.dom(this.element).hasText('template block text');
    });
  });
});
define("moviebooking/tests/test-helper", ["moviebooking/app", "moviebooking/config/environment", "qunit", "@ember/test-helpers", "qunit-dom", "ember-qunit"], function (_app, _environment, QUnit, _testHelpers, _qunitDom, _emberQunit) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"moviebooking/app",0,"moviebooking/config/environment",0,"qunit",0,"@ember/test-helpers",0,"qunit-dom",0,"ember-qunit"eaimeta@70e063a35619d71f
  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));
  (0, _qunitDom.setup)(QUnit.assert);
  (0, _emberQunit.start)();
});
define("moviebooking/tests/unit/adapters/applog-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Adapter | applog', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let adapter = this.owner.lookup('adapter:applog');
      assert.ok(adapter);
    });
  });
});
define("moviebooking/tests/unit/controllers/adam-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Controller | adam', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // TODO: Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:adam');
      assert.ok(controller);
    });
  });
});
define("moviebooking/tests/unit/controllers/adampage-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Controller | adampage', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // TODO: Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:adampage');
      assert.ok(controller);
    });
  });
});
define("moviebooking/tests/unit/controllers/application-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Controller | application', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // TODO: Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:application');
      assert.ok(controller);
    });
  });
});
define("moviebooking/tests/unit/controllers/avatarbook-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Controller | avatarbook', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // TODO: Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:avatarbook');
      assert.ok(controller);
    });
  });
});
define("moviebooking/tests/unit/controllers/avathar-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Controller | avathar', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // TODO: Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:avathar');
      assert.ok(controller);
    });
  });
});
define("moviebooking/tests/unit/controllers/bookticket-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Controller | bookticket', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // TODO: Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:bookticket');
      assert.ok(controller);
    });
  });
});
define("moviebooking/tests/unit/controllers/mainpage-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Controller | mainpage', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // TODO: Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:mainpage');
      assert.ok(controller);
    });
  });
});
define("moviebooking/tests/unit/controllers/payamount-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Controller | payamount', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // TODO: Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:payamount');
      assert.ok(controller);
    });
  });
});
define("moviebooking/tests/unit/models/applog-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Model | applog', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('applog', {});
      assert.ok(model);
    });
  });
});
define("moviebooking/tests/unit/routes/adam-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | adam', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:adam');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/adampage-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | adampage', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:adampage');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/avatar-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | avatar', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:avatar');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/avatarbook-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | avatarbook', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:avatarbook');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/avathar-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | avathar', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:avathar');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/bookticket-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | bookticket', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:bookticket');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/mainpage-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | mainpage', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:mainpage');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/payamount-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | payamount', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:payamount');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/sky-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | sky', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:sky');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/successful-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | successful', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:successful');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/routes/wonder-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Route | wonder', function (hooks) {
    (0, _helpers.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:wonder');
      assert.ok(route);
    });
  });
});
define("moviebooking/tests/unit/serializers/applog-test", ["qunit", "moviebooking/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"moviebooking/tests/helpers"eaimeta@70e063a35619d71f
  (0, _qunit.module)('Unit | Serializer | applog', function (hooks) {
    (0, _helpers.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let serializer = store.serializerFor('applog');
      assert.ok(serializer);
    });
    (0, _qunit.test)('it serializes records', function (assert) {
      let store = this.owner.lookup('service:store');
      let record = store.createRecord('applog', {});
      let serializedRecord = record.serialize();
      assert.ok(serializedRecord);
    });
  });
});
define('moviebooking/config/environment', [], function() {
  var prefix = 'moviebooking';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(decodeURIComponent(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

require('moviebooking/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
