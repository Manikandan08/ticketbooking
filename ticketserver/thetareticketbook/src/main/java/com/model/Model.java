package com.model;

import java.sql.ResultSet;

import org.json.simple.JSONObject;

public class Model {
	
	private String email; 
	private String number;
//	private String id;
	
	public String getEmail() {
		return email;
	}
    public void setEmail(String email) {
    	this.email = email;
    }
    public String getNumber() {
    	return  number;
    }
    
    public void setNumber(String number) {
    	this.number = number;
    }
//    public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
	public Model(JSONObject userdetail) {
		System.out.println(userdetail);
		
		if ((String) userdetail.get("email") != null) {
			this.setEmail((String) userdetail.get("email"));
		}
		if ((String) userdetail.get("number") != null) {
			this.setNumber((String) userdetail.get("number"));
		}
		
//		if ((String) userdetail.get("id") != null) {
//			System.out.println((String) userdetail.get(id));
//			this.setId((String) userdetail.get("id"));
//			
//		}
	}

	public  Model(ResultSet resultset) throws Exception {
		try {
			
			this.setEmail(resultset.getString("email"));
			this.setNumber(resultset.getString("number"));
//			this.setId(resultset.getString("id"));
		}  catch (Exception e) {
			throw e;
		}
	}

	public JSONObject getjson() {
		JSONObject jsonobj = new JSONObject();
//		jsonobj.put("id", this.getId());
		
		jsonobj.put("email", this.getEmail());
		jsonobj.put("number", this.getNumber());
		return jsonobj;

	}

	
}
