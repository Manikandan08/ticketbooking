package com.main;

import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.model.Model;
import com.workflow.workflow;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

//import org.apache.commons.io.IOUtils;

/**
 * Servlet implementation class Logform
 */
@WebServlet("/applogs")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public Main() {
		// TODO Auto-gener ated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
   int idval = 1;
   JSONObject finalget = new JSONObject();
		PrintWriter writer = response.getWriter();
		try {
			Cookie cookies[] = request.getCookies();
			String name = "";
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("email")) {
					name = cookie.getValue();
					writer.print(name);
					idval++;
					
				}
   			}
		} catch (Exception e) {
			throw e;
		} finally {
			writer.close();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		 response.setContentType("application/json");

		StringBuilder build = new StringBuilder();
		BufferedReader bufferReader = null;
		try {
			if (build != null) {
				InputStream value = request.getInputStream();
				bufferReader = new BufferedReader(new InputStreamReader(value));
				String storeval;
				while ((storeval = bufferReader.readLine()) != null) {
					build.append(storeval);
				}
			}

		} catch (IOException e) {
			throw e;
		} finally {
			if (bufferReader != null) {
				try {
					bufferReader.close();
				} catch (IOException e) {
					throw e;
				}
			}
		}	
		String valstr = build.toString();
		System.out.println(valstr);
		try {
			JSONParser parse = new JSONParser();
			JSONObject objval = (JSONObject) parse.parse(valstr);
			JSONObject userdetail = (JSONObject) objval.get("applog"); 
			Model userget = new Model(userdetail);
			workflow userflow = new  workflow();
//			if((String) userdetail.get("firstName")!=null&&(String) userdetail.get("password")!=null&&(String) userdetail.get("email")!=null) {
				Model signupdata = userflow.signupValue(userget);
//			} else {
//				Model  dbuserdata= userflow.signInUser(userget);
				
				
				Cookie ck = new Cookie("email", String.valueOf(signupdata.getEmail()));
				response.addCookie(ck);
				JSONObject jsonobjj = new JSONObject();
				jsonobjj.put("id", 1);
				JSONObject jsonobj = new JSONObject();
				jsonobj.put("applog", jsonobjj);
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().append(jsonobj.toString());
					
//			}
//			UserModel userget = new UserModel(userdetail);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		
		}
	
	}
}
